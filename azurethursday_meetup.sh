
#https://docs.microsoft.com/en-us/azure/container-registry/container-registry-oci-artifacts

ACR_NAME=azuregiovedi
REGION=southcentralus
REGISTRY=$ACR_NAME.azurecr.io
REPO=net-monitor
TAG=v1
IMAGE=$REGISTRY/${REPO}:$TAG

#az acr create -l $REGION --zone-redundancy Enabled --sku Premium -g resources -n $ACR_NAME
az group create -n $ACR_NAME
az acr create \
  --resource-group $ACR_NAME \
  --name $ACR_NAME \
  --zone-redundancy enabled \
  --sku Premium \
  --output jsonc

#notation doesn't work yet with ACR https://github.com/notaryproject/notation/issues/119
#USER_NAME="00000000-0000-0000-0000-000000000000"
#PASSWORD=$(az acr login --name $ACR_NAME --expose-token --output tsv --query accessToken)

#https://stackoverflow.com/questions/67642620/docker-credential-desktop-not-installed-or-not-available-in-path
#change ~/.docker/config.json to credStore on Mac

#GHCR 
#echo $GITHUB_PAT | oras login https://ghcr.io -u GITHUB_USERNAME --password-stdin

#local registry
export PORT=5001
export REGISTRY=localhost:${PORT}
export REPO=${REGISTRY}/ams0/net-monitor
export IMAGE=${REPO}:v1
docker run -d --rm -p ${PORT}:5000 ghcr.io/oras-project/registry:v0.0.3-alpha


#build and push
docker build -t $IMAGE https://github.com/wabbit-networks/net-monitor.git#main
docker push $IMAGE

#sign with first cert
notation cert generate-test --default "azurethursday"
notation sign $IMAGE

notation list $IMAGE
notation verify $IMAGE

#Add the cert
notation cert add --name "azurethursday" "/Users/alessandro/Library/Application Support/notation/certificate/azurethursday.crt"
notation verify $IMAGE

#generate and sign with a second cert
notation cert generate-test --default "microsoft"
notation cert add --name "microsoft" "/Users/alessandro/Library/Application Support/notation/certificate/microsoft.crt"
notation sign $IMAGE
notation verify $IMAGE

Check signature
oras discover -o tree $IMAGE

#generate sbom for image docker sbom=syft
docker sbom $IMAGE -o spdx-json > image-sbom.json

oras push $REPO \
  --artifact-type 'sbom/example' \
  --subject $IMAGE \
  ./image-sbom.json:application/json

SBOM_DIGEST=$(oras discover -o json \
    --artifact-type sbom/example \
    $IMAGE | jq -r ".references[0].digest")

#sign the sbom
notation sign $REPO@$SBOM_DIGEST
oras discover -o tree $IMAGE

#use Snyk to scan the image
docker scan --json $IMAGE > scan-results.json
oras push $REPO \
  --artifact-type application/vnd.org.snyk.results.v0 \
  --subject $IMAGE \
  scan-results.json
  
SCAN_DIGEST=$(oras discover -o json \
    --artifact-type application/vnd.org.snyk.results.v0 \
    $IMAGE | jq -r ".references[0].digest")
notation sign $REPO@$SCAN_DIGEST

oras discover -o tree $IMAGE


#Cosign in AKS

#brew install cosign
export COSIGN_PASSWORD=$(pwgen -y 64 1)
cosign generate-key-pair

kubectl create namespace cosign-system
kubectl create secret generic mysecret -n \
cosign-system --from-file=cosign.pub=./cosign.pub

helm repo add sigstore https://sigstore.github.io/helm-charts
#helm repo update
helm install cosigned -n cosign-system sigstore/cosigned --devel --set cosign.secretKeyRef.name=mysecret

kubectl create ns securens
kubectl label ns securens cosigned.sigstore.dev/include="true"

docker.io/securesystemsengineering/testimage:signed
docker.io/securesystemsengineering/testimage:unsigned